## 3.141.59-p40
 + **Changes:** https://github.com/elgalu/docker-selenium/compare/3.141.59-p39...3.141.59-p40 (2020-05-02)
    + Olivier Berger (1):
        * [0cee743] Proposed fix for audio recording issue (https://github.com/elgalu/docker-selenium/issues/147#issuecomment-613765353) (#381)
 + **Image tag details:**
    + Selenium version: 3.141.59 (82b03c358b)
    + Chrome stable:  81.0.4044.129
    + Firefox stable: 75.0
    + Geckodriver: 0.26.0
    + Chromedriver: 81.0.4044.69 (6813546031a4bc83f717a2ef7cd4ac6ec1199132)
    + Java: OpenJDK Java 1.8.0_252-8u252-b09-1
    + Default Timezone: Europe/Berlin
    + FROM ubuntu:xenial-20190904
    + Python: 3.5.2
    + Tested on kernel CI  host: 4.15.0-1028-gcp x86_64
    + Built at CI  host with: Docker version 18.06.0, build 0ffa825
    + Built at CI  host with: Docker Compose version 1.23.1, build b02f1306
    + Image version: 3.141.59-336
    + Image size: 1.54GB
    + Digest: sha256:aec5374afbf45972f1c1bd9c572feea7bbe3ec1911b34cd54a30083f1cbd455b

